<?php
class Bitm{
    public $labname;
    public $window;
    public $door;
    public $chair;
    public $table;
    public $whiteboard;
    public function __construct($lab)
    {
        $this->labname = $lab;
        echo "<b>".$lab."</b><br>";
    }
    public function coolTheAir(){
        echo 'Ac is cooling the room';
    }
    public function compute($nil){
        $this->nil = $nil;
        echo $nil;
    }
    public function show(){
        echo $this->chair."<br>";
        echo $this->table."<br>";
        echo $this->door."<br>";
        echo $this->whiteboard."<br>";
        echo $this->window."<br>";
    }
    public function setChair($chair)
    {
        $this->chair = $chair;

    }
    public function setTable($table)
    {
        $this->table = $table;
    }
    public function setDoor($door){
        $this->door=$door;
    }
    public function setWhiteboard($whiteboard)
    {
        $this->whiteboard = $whiteboard;
    }
    public function setWindow($window)
    {
        $this->window = $window;
    }
    public function setAll(){
        $this->setChair("I'm a Chair from parent");
        $this->setTable("I'm a Table from parent");
        $this->setDoor('I am a door');
        $this->setWhiteboard("I'm  a whiteboard");
        $this->setWindow("I'm a window");
    }
}
$lab = new Bitm("Dhaka Lab");
$lab->setAll();
$lab->show();

class BitmLab2 extends Bitm{
    public function setChair($chair) {
        parent::setChair("This chair modified from child");
    }
    public function setTable($table) {
        parent::setTable("This Table modified from child");
    }
}
$lab2 = new BitmLab2("Chittagong Lab");
$lab2->setAll();
$lab2->show();


