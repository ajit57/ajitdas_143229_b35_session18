<?php
class AnyClass{
    public $myProperty1;
    public $myProperty2;
    public $otherProperty;
    public function __sleep()
    {
//       echo "I'm inside sleep magic method <br>";
        return array("myProperty1", "myProperty2");
    }
    public function __wakeup()
    {
        echo "I'm inside wakeup magic method <br>";
    }
    public function __toString()
    {
        echo $this->myProperty1."<br>";
        echo $this->myProperty2."<br>";
        echo $this->otherProperty."<br>";
        return "you can not echo an object directly";
    }
}
$obj = new AnyClass();
echo $obj;