<?php

class AnyClass{
    public $myProperty1;
    public $myProperty2;
    public $otherProperty;
    public function __sleep()
    {
//       echo "I'm inside sleep magic method <br>";
       return array("myProperty1", "myProperty2");
    }
    public function __wakeup()
    {
        echo "I'm inside wakeup magic method <br>";
    }
}
$obj = new AnyClass();
$serializeString = serialize($obj);
echo $serializeString."<br>";
$obj2 = unserialize($serializeString);
print_r($obj2);