<?php
class ImAClass{
    public static $myMsg;
    public static function message() {
        echo self::$myMsg;
    }
    public function __construct($value)
    {
        echo $value . "<br>";
    }
    public function __destruct()
    {
        echo "good Bye <br>";
    }

    public function __call($nam, $argument)
    {
        echo "You are calling a undefined method ".$nam."<br>";
        print_r($argument);
    }
    public static function __callStatic($name, $arguments)
    {
        echo $name."<br>";
        print_r($arguments);
    }
    public function __get($name)
    {
        echo $name."<br>";
    }
    public function __set($name, $value)
    {
        echo $name."<br>";
        echo $value."<br>";
    }
    public function __isset($name)
    {
        echo $name."<br>";
    }
    public function __unset($name)
    {
        echo $name."<br>";
    }
    public function __sleep()
    {
        echo "I am now sleeping";
    }
}
ImAClass::$myMsg = "Image Has been deleted"."<br>";
echo ImAClass::messages(859, "This is a problem");

$obj = new ImAClass("Hello Class Initiated!");
$obj->join(4,5,6,7,8);
echo $obj->window;
$obj->door = "eta ekta doroja";
isset($obj->chair);
unset($obj->table);